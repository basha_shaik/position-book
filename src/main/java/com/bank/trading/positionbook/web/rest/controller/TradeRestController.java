package com.bank.trading.positionbook.web.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bank.trading.positionbook.model.Account;
import com.bank.trading.positionbook.model.RequestModel;
import com.bank.trading.positionbook.service.PositionBookService;

@RestController
public class TradeRestController {

	@Autowired
	private PositionBookService service = new PositionBookService();

	@GetMapping("/account/{id}")
	public ResponseEntity<Account> getAccount(@PathVariable String id) {
			return new ResponseEntity<>(service.getPositionBook().getSummary(id).getAccount(), HttpStatus.OK);
	}
	
	
	@PostMapping("/trade")
	public ResponseEntity<Void> postTrade( @RequestBody RequestModel requestModel) {
			service.getPositionBook().processTradeRequest(requestModel);
			return ResponseEntity.ok().build();
	}

	public void setService(PositionBookService service) {
		this.service = service;
	}
}
